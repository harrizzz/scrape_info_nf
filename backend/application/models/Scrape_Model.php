<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scrape_Model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function insert_db($table_name, $data) {
    $this->db->insert($table_name, $data);
  }

  public function update_db($table_name, $data, $nim) {
    $this->db->update($table_name, $data, $nim);
  }

  public function search_db($table_name, $data, $custom_search = null) {
    $this->db->where($data);
    $this->db->order_by('tahun', 'desc');
    if (!empty($custom_search)) {
      $q = $this->db->select($custom_search);
    }
    $q = $this->db->get($table_name);
    return $q->result();
  }

  public function single_search_db($table_name, $data, $custom_search = null) {
    $this->db->where($data);
    if (!empty($custom_search)) {
      $q = $this->db->select($custom_search);
    }
    $q = $this->db->get($table_name);
    return $q->row();
  }
}