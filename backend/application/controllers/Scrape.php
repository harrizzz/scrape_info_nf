<?php
header('Access-Control-Allow-Origin:*');
defined('BASEPATH') OR exit('No direct script access allowed');

class Scrape extends MY_Controller {
	private $mainURL = "https://info.nurulfikri.ac.id/";

	public function __construct() {
		parent::__construct();
		require_once(APPPATH . "third_party/dom.php");
		$this->load->model('Scrape_Model');
	}

	public function index() {
		echo 'you lost! go somewhere else';
		//0110213026 - dawaipagi
	}

	public function login($nim = null, $password = null) {
		if(!empty($nim) && !empty($password)) {
			$this->session->set_userdata([
				'nim' => $nim,
				'password' => $password
			]);
			if ($this->log_into()) {
				$this->mahasiswa();
				$this->biaya();
				$this->sejarah();
				$this->getUserData();
				// echo json_encode('Hello World');
			} else {
				echo json_encode('Something went wrong');
			}
		} else {
			echo json_encode('No!');
		}
	}

	private function log_into() {
		$url = $this->mainURL . "?slnt=prclogin&slntx=PrcLoginMhsw&mnux=mhsw&DM=&Login=".$this->session->userdata('nim')."&Password=".$this->session->userdata('password')."&Submit=Login";

		$html = parent::curl_run($url);
		$res = str_get_html($html);

		if (@$res->find('font[color=yellow]', 0)->plaintext === 'Gagal Login') {
			return false;
		} else {
			return true;
		}
	}

	private function mahasiswa() {
		if($this->Scrape_Model->single_search_db('mahasiswa', ['nim' => $this->session->userdata('nim')])) return;

		$url = $this->mainURL . "?slnt=prclogin&slntx=PrcLoginMhsw&mnux=mhsw&DM=&Login=".$this->session->userdata('nim')."&Password=".$this->session->userdata('password')."&Submit=Login";

		$html = parent::curl_run($url);
		$res = str_get_html($html);
		
		$tmp_img = explode("../", $res->find('img', 1)->src);
		$img = $this->mainURL. $tmp_img[count($tmp_img) - 1];
		$program_studi = explode('- ',$res->find('table.bsc tr', 3)->find('td', 3)->plaintext)[1];
		$tmp_status = trim(str_replace('&nbsp;', '', $res->find('table.bsc tr', 4)->find('td', 3)->plaintext));
		$status = ($tmp_status === 'Aktif' ? 1 : 0);
		$pembimbing_akademik = trim(str_replace('&nbsp;', '', $res->find('table.bsc tr', 5)->find('td', 3)->plaintext));
		
		$data = [
			'nim' => $res->find('table.bsc tr', 2)->find('td', 1)->plaintext,
			'nama' => $res->find('table.bsc tr', 3)->find('td', 1)->plaintext,
			'url_foto' => $img,
			'program_studi' => $program_studi,
			'angkatan' => $res->find('table.bsc tr', 4)->find('td', 1)->plaintext,
			'pembimbing_akademik' => $pembimbing_akademik,
			'status' => $status
		];

		$this->Scrape_Model->insert_db('mahasiswa', $data);
	}

	private function biaya() {
		$url = $this->mainURL . "?mnux=mhsw&sub=KEU";

		$html = parent::curl_run($url);
		$res = str_get_html($html);

		$tmp_table = $res->find('p', 2)->find('table.bsc', 0);
		
		$tr_length = count($tmp_table->childNodes()) - 1;
		$thn_akademik = $tmp_table->find('tr', $tr_length - 1)->find('td', 1)->plaintext;

		$biaya = implode('',explode(',', filter_var($tmp_table->find('tr', $tr_length - 1)->find('td', 4), FILTER_SANITIZE_NUMBER_INT)));
		
		$data = [
			'thn_akademik' => (int) $thn_akademik,
			'biaya' => (int) $biaya
		];

		$this->Scrape_Model->update_db('mahasiswa', $data, ['nim' => $this->session->userdata('nim')]);
	}

	public function sejarah() {
		$url = $this->mainURL . "?mnux=mhsw&sub=HIST";

		$html = parent::curl_run($url);
		$res = str_get_html($html);

		$tmp_table = $res->find('table.bsc', 1);
		$tr_length = count($tmp_table->childNodes()) - 1;
		$tmp = [];

		for ($x = 2; $x <= $tr_length; $x++) {
			$xyz = [];

			for ($z = 1; $z <= 6; $z++) {
				array_push($xyz, @$res->find('table.bsc', 1)->find('tr', $x)->find('td', $z)->plaintext);
			}

			array_push($tmp, $xyz);
		}

		for ($y = 0; $y < count($tmp); $y++) {
			if (!empty($tmp[$y][0])) {
				$data = [
					'kode' => $tmp[$y][0],
					'matakuliah' => $tmp[$y][1],
					'sks' => $tmp[$y][2],
					'grade' => $tmp[$y][3],
					'bobot' => $tmp[$y][4],
					'tahun' => $tmp[$y][5]
				];
				$res = $this->Scrape_Model->single_search_db('sejarah', ['tahun' => $tmp[$y][5], 'matakuliah' => $tmp[$y][1]], 'tahun, matakuliah');
				if (count($res) > 0) {
					$this->Scrape_Model->update_db('sejarah', $data, $this->session->userdata('nim'));
				} else {
					$data['nim'] = $this->session->userdata('nim');
					$this->Scrape_Model->insert_db('sejarah', $data);
				}
			}
		}
	}

	private function getUserData() {
		$this->load->model('Scrape_Model');
		$res = $this->Scrape_Model->single_search_db('mahasiswa', ['nim' => $this->session->userdata('nim')], 'nim, nama, url_foto, program_studi, biaya');
		echo json_encode($res);
	}

	public function getHistoryUser($nim = null) {
		if (is_null($nim)) return;
		$this->load->model('Scrape_Model');
		$res = $this->Scrape_Model->search_db('sejarah', ['nim' => $nim]);
		$arr;
		for ($x = 0; $x < count($res); $x++) {
			$arr[$x] = [
				'id_sejarah' => $res[$x]->id_sejarah,
				'kode' => $res[$x]->kode,
				'matakuliah' => $res[$x]->matakuliah,
				'sks' => $res[$x]->sks,
				'grade' => $res[$x]->grade,
				'bobot' => $res[$x]->bobot,
				'tahun' => $res[$x]->tahun,
				'nim' => $res[$x]->nim,
			];
		}
		echo json_encode($res);
	}

	public function khs_menu() {
		$this->do_run('khs');
	}

	public function logout() {
		$this->session->unset_userdata(['nim', 'password', 'cookie']);
	}
}
