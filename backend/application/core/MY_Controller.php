<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	private $userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/56.0.2924.76 Chrome/56.0.2924.76 Safari/537.36";

	public function __construct() {
		parent::__construct();
	}

	protected function curl_run($url = null) {
		if (empty($url)) exit;

		//initialize
		$curl = curl_init();

		//curl options
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->userAgent);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);

		if($url){
			//set cookie
			if(!empty($_SESSION['cookie'])) curl_setopt($curl, CURLOPT_COOKIE, $_SESSION['cookie']);
		}

		//execute curl and get result
		$result = curl_exec($curl);

		//if fix_url is equal to url (method) do cookie thing
		if($url){
			if(empty($_SESSION['cookie'])){
				//get header (we only extract cookie from it)
				$session = strpos($result, 'PHPSESSID=');
				if($session > -1){
					$tmp = substr($result, $session);
					$cookie = explode(';', $tmp);
					$_SESSION['cookie'] = $cookie[0];
					// var_dump($cookie[0]);
				}
			}
		} 

		//catch error if any
		if(curl_errno($curl)) die("Scrape error: ".curl_error($curl));

		//close connection from target
		curl_close($curl);

		//print result
		return $result;
	}

	protected function do_run($sub) {
		$url  = $this->mainURL . "?mnux={$this->mnux}&sub=".strtoupper($sub);

		$html = $this->curl_run($url);

		$res = str_get_html($html);

		echo $res;

		echo $res->find('select[name=__TahunID]')[0]->plaintext;
	}
}
