-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2017 at 09:45 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scrape`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `url_foto` varchar(100) DEFAULT NULL,
  `program_studi` varchar(40) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `pembimbing_akademik` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `biaya` int(100) DEFAULT NULL,
  `thn_akademik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `url_foto`, `program_studi`, `angkatan`, `pembimbing_akademik`, `status`, `biaya`, `thn_akademik`) VALUES
('0110216001', 'Muhammad Alfathan Harriz', 'https://info.nurulfikri.ac.id/foto/0110216001.jpeg', ' Teknik Informatika', 20161, 'Ahmad Rio A, M.Si', 1, 3000000, 20171);

-- --------------------------------------------------------

--
-- Table structure for table `sejarah`
--

CREATE TABLE `sejarah` (
  `id_sejarah` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `matakuliah` varchar(50) NOT NULL,
  `sks` int(3) NOT NULL,
  `grade` varchar(3) NOT NULL,
  `bobot` double NOT NULL,
  `tahun` int(11) NOT NULL,
  `nim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sejarah`
--

INSERT INTO `sejarah` (`id_sejarah`, `kode`, `matakuliah`, `sks`, `grade`, `bobot`, `tahun`, `nim`) VALUES
(1, 'IE11003', 'PENGANTAR TEKNOLOGI INFORMASI', 2, 'B+', 3.3, 20161, 110216001),
(2, 'IE11006', 'ORGANISASI DAN ARSITEKTUR KOMPUTER', 3, 'B-', 2.7, 20161, 110216001),
(3, 'IE11007', 'MATEMATIKA DISKRIT', 3, 'B-', 2.7, 20161, 110216001),
(4, 'IE21003', 'BASIS DATA II', 3, 'A', 4, 20161, 110216001),
(5, 'IE21004', 'TATA KELOLA TEKNOLOGI INFORMASI', 3, 'B+', 3.3, 20161, 110216001),
(6, 'NF11002', 'MATEMATIKA DASAR', 2, 'B-', 2.7, 20161, 110216001),
(7, 'NF11003', 'BAHASA INDONESIA', 2, 'C', 2, 20161, 110216001),
(8, 'IE22003', 'DATA WAREHOUSE', 2, 'C', 2, 20162, 110216001),
(9, 'NF21003', 'STATISTIK DAN PROBABILITAS', 3, 'B', 3, 20162, 110216001),
(10, 'TI034202', 'ANALISIS NUMERIK', 3, 'B', 3, 20162, 110216001);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `sejarah`
--
ALTER TABLE `sejarah`
  ADD PRIMARY KEY (`id_sejarah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sejarah`
--
ALTER TABLE `sejarah`
  MODIFY `id_sejarah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
